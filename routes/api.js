var express = require('express');
var router = express.Router();
var { Car } = require('../models');
var multer = require('multer');

// file upload configuration
const storage = multer.diskStorage({
  destination: (req, file, cb) => cb(null, './public/images'),
  filename: (req, file, cb) => {
    const fileName = `${Date.now()}${Math.floor(Math.random()*1E6)}.${file.originalname.split('.').pop()}`; //random filename
    cb(null, fileName)
  }
})

// multer configuration for file upload
const upload = multer({ storage: storage })

/* GET users listing. */
router.get('/cars', async function(req, res, next) {
  const cars = await Car.findAll();
  res.json(cars);
  //res.send('respond with a resource');
});

router.post('/cars',upload.single('photo'), async function(req, res, next) {
  
  const {name, price, size} = req.body;
  const filename = req.file.filename;
  
  const car = await Car.create({ name,price,size,filename})

  res.send(car);
});

router.put('/cars', upload.single('image'), async function(req, res, next) {
  const carPick = await Car.findByPk(req.body.id);
  const { name, price, size} = req.body;
  const filename = req.file ? req.file.filename : carPick.filename; //ternary operator

  await carPick.update({name,price,size,filename})
  res.send('Data berhasil diubah')
});

router.delete('/cars', upload.none() ,async function(req, res, next) {
  const car1 = await Car.findByPk(req.body.id);
  car1.destroy();
  res.send('Data Berhasil Di Hapus');
});

module.exports = router;
