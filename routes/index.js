var express = require('express');
var router = express.Router();

const {Car} = require('../models');

/* GET home page. */
router.get('/',async function(req, res, next) {
  const cars = await Car.findAll({order:[['id','ASC']]})
  res.render('index', { cars });
});

router.get('/add-new', function(req, res, next) {
  res.render('add-new');
});

router.get('/addCar', function(req, res, next) {
  res.render('add-new');
});

router.get('/editCar/:id', async function(req, res, next) {
  const mobil = await Car.findByPk(req.params.id)
  res.render('edit-car', { mobil });
});

module.exports = router;
