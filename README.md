# Challenge 5

Challenge 5 Binar Academy

## ERD Database
![ERD](/public/images/erd.png)

## Link untuk masing-masing halaman
```http
Halaman index         = http://localhost:8000
Halaman add new car   = http://localhost:8000/addCar
Halaman edit car      = http://localhost:8000/addCar/id
```

## Link REST API
```http
GET all car data         = http://localhost:8000/api/cars
GET car data by ID       = http://localhost:8000/api/cars/:id
PUT                      = http://localhost:8000/api/cars
POST                     = http://localhost:8000/api/cars/:id
DELETE                   = http://localhost:8000/api/cars/:id
```

## Contoh Request and Response 

### GET all car data 

#### Request :
```http
http://localhost:8000/api/cars
```
#### Response :
```javascript
[
    {
        "id": 7,
        "name": "cars3",
        "price": 3000,
        "size": 1,
        "filename": "165061794263541513.png",
        "createdAt": "2022-04-22T08:59:02.639Z",
        "updatedAt": "2022-04-22T08:59:02.639Z"
    },
    {
        "id": 8,
        "name": "coba2",
        "price": 12,
        "size": 2,
        "filename": "1650617996588669382.png",
        "createdAt": "2022-04-22T08:59:56.589Z",
        "updatedAt": "2022-04-22T08:59:56.589Z"
    },
    {
        "id": 10,
        "name": "coba5",
        "price": 5555,
        "size": 1,
        "filename": "165061868992732788.png",
        "createdAt": "2022-04-22T09:11:29.932Z",
        "updatedAt": "2022-04-22T09:11:29.932Z"
    },
    {
        "id": 11,
        "name": "coba6",
        "price": 666,
        "size": 1,
        "filename": "1650618710514111634.png",
        "createdAt": "2022-04-22T09:11:50.515Z",
        "updatedAt": "2022-04-22T09:11:50.515Z"
    },
    {
        "id": 12,
        "name": "asdf",
        "price": 123,
        "size": 1,
        "filename": "165061890472533654.png",
        "createdAt": "2022-04-22T09:15:04.727Z",
        "updatedAt": "2022-04-22T09:15:04.727Z"
    },
    {
        "id": 13,
        "name": "qwer",
        "price": 123,
        "size": 1,
        "filename": "1650618924256494248.png",
        "createdAt": "2022-04-22T09:15:24.259Z",
        "updatedAt": "2022-04-22T09:15:24.259Z"
    },
    {
        "id": 14,
        "name": "asdfe",
        "price": 1234,
        "size": 1,
        "filename": "1650618991708303365.png",
        "createdAt": "2022-04-22T09:16:31.711Z",
        "updatedAt": "2022-04-22T09:16:31.711Z"
    },
    {
        "id": 16,
        "name": "lala",
        "price": 100000,
        "size": 1,
        "filename": "1650621125897806452.png",
        "createdAt": "2022-04-22T09:52:05.903Z",
        "updatedAt": "2022-04-22T09:52:05.903Z"
    },
    {
        "id": 6,
        "name": "cars",
        "price": 10000,
        "size": 1,
        "filename": "1650614578616594803.png",
        "createdAt": "2022-04-22T08:02:58.621Z",
        "updatedAt": "2022-04-22T10:45:23.225Z"
    }
]
```
