const showPopup = (id)=>{
    document.querySelector('#delete-id').value = id;
}

const submitDel = (id)=>{
    const carId = document.querySelector('#delete-id').value;
    fetch('/api/cars', {
        method: 'DELETE', 
        body: JSON.stringify({
            id: carId
        }),
        headers: {
            'Content-Type': 'application/json',
        },
    })
    sessionStorage.setItem('alert-delete', 'Data berhasil di delete')
    location.reload();
}

document.addEventListener('DOMContentLoaded',() =>{
    if(sessionStorage.getItem('alert-success')){
        const msg = sessionStorage.getItem('alert-success');
        const alert = document.getElementById('alert-success');
        alert.style.display = 'block';
        alert.innerText = msg;
        sessionStorage.removeItem('alert-success');

        setTimeout(() => {
            alert.style.display = 'none';
        },2000)  
    }

    if(sessionStorage.getItem('alert-delete')){
        const msg = sessionStorage.getItem('alert-delete');
        const alert = document.getElementById('alert-delete');
        alert.style.display = 'block';
        alert.innerText = msg;
        sessionStorage.removeItem('alert-delete');

        setTimeout(() => {
            alert.style.display = 'none';
        },2000)  
    }

    if(sessionStorage.getItem('alert-edit')){
        const msg = sessionStorage.getItem('alert-edit');
        const alert = document.getElementById('alert-edit');
        alert.style.display = 'block';
        alert.innerText = msg;
        sessionStorage.removeItem('alert-edit');

        setTimeout(() => {
            alert.style.display = 'none';
        },2000)  
    }
   
});



