const submit = async () => {
    const fromData = new FormData();
    fromData.append('id', document.querySelector('#car-id').value);
    fromData.append('name', document.querySelector('#inputName').value);
    fromData.append('price', document.querySelector('#inputPrice').value);
    fromData.append('size', document.querySelector('#inputSize').value);
    fromData.append('photo', document.querySelector('#inputFile').files[0]);

    await fetch('/api/cars', {
        method: 'PUT',
        body: fromData
    })
    
    sessionStorage.setItem('alert-edit', 'Data berhasil diedit')
    window.location.href = "/";
}