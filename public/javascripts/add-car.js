const submit = async () => {
    const fromData = new FormData();
    fromData.append('name', document.querySelector('#inputName').value);
    fromData.append('price', document.querySelector('#inputPrice').value);
    fromData.append('size', document.querySelector('#inputSize').value);
    fromData.append('photo', document.querySelector('#inputFile').files[0]);

    await fetch('/api/cars', {
        method: 'POST',
        body: fromData
    })
    
    sessionStorage.setItem('alert-success', 'Data berhasil disimpan')
    window.location.href = "/";
}